package main

import (
    "bufio"
    "fmt"
    "log"
    "os"
    "strings"

    "gitlab.com/jacobdavids/robots.git/robot"
    "gitlab.com/jacobdavids/robots.git/validate"
)

// Logger is a global logger used to show error messages
var Logger = log.New(os.Stdout, " ", log.Lshortfile)

func parseCommand(input string) (args []string, err error) {
    // Strip new line character from input
    input = strings.Replace(input, "\n", "", -1)

    // Split string by space character
    args = strings.Fields(input)

    // Validate command
    err = validate.ValidateCommand(args)
    if err != nil {
        return
    }
    return
}

func main() {
    // Print instructions to user
    fmt.Println("Welcome to the 2017 Toy Robot Simulator. Written by Jacob Davids.")
    fmt.Println("-----------------------------------------------------------------")

    reader := bufio.NewReader(os.Stdin)

    r := robot.CreateRobot()

    for {
        fmt.Print("Please enter a command> ")

        // Read input from user
        input, _ := reader.ReadString('\n')

        // Parse user input
        args, err := parseCommand(input)
        if err != nil {
            Logger.Println(err)
            continue
        }

        // Check if robot has been placed on table
        err = robot.GetRobotStatus(r, args)
        if err != nil {
            Logger.Println(err)
            continue
        }

        // No errors found, proceed with processing command
        err = robot.ProcessCommand(r, args)
        if err != nil {
            Logger.Println(err)
        }
    }
}