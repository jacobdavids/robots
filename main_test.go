package main

import (
    "reflect"
    "testing"

    "gitlab.com/jacobdavids/robots.git/robot"
    "gitlab.com/jacobdavids/robots.git/validate"
)

// Test creation of new robot instance
func TestRobotCreation(t *testing.T) {
    r := robot.CreateRobot()

    if r.Coordinate.X != 0 {
        t.Error("Expected", 0, "got", r.Coordinate.X)
    }

    if r.Coordinate.Y != 0 {
        t.Error("Expected", 0, "got", r.Coordinate.Y)
    }

    if r.Direction != 0 {
        t.Error("Expected", 0, "got", r.Direction)
    }
} 

type rawTestCommand struct {
    raw string
    parsedArgs []string
    output error
}

var rawTestCommands = []rawTestCommand {
    // Invalid commands
    {"", []string{}, validate.ErrInvalidCommand},
    {"   ", []string{}, validate.ErrInvalidCommand},
    {"TEST", []string{"TEST"}, validate.ErrInvalidCommand},
    {"MOVE    .", []string{"MOVE", "."}, validate.ErrInvalidCommand},
    {"PLACE 0,0,NORTH,", []string{"PLACE", "0,0,NORTH,"}, validate.ErrInvalidCommand},
    {"PLACE -1,0,NORTH", []string{"PLACE", "-1,0,NORTH"}, validate.ErrInvalidCoordinates},
    {"PLACE 5,0,NORTH", []string{"PLACE", "5,0,NORTH"}, validate.ErrInvalidCoordinates},
    {"PLACE 1m,0,NORTH",[]string{"PLACE", "1m,0,NORTH"}, validate.ErrInvalidCoordinates},

    // Valid commands
    {"PLACE 0,0,NORTH", []string{"PLACE", "0,0,NORTH"}, nil},
    {"MOVE", []string{"MOVE"}, nil},
    {"LEFT", []string{"LEFT"}, nil},
    {"RIGHT", []string{"RIGHT"}, nil},
    {"REPORT", []string{"REPORT"}, nil},
    {"HELP", []string{"HELP"}, nil},
    {"MOVE      ", []string{"MOVE"}, nil},
}

// Test raw command parsing and validation
func TestCommandParseAndValidation(t *testing.T) {
    for _, c := range rawTestCommands {
        args, r := parseCommand(c.raw)
        // Check return value is as expected
        if r != c.output {
            t.Error("For", c.raw, "expected", c.output, "got", r)
        }
        // Check parsed args is as expected
        if args == nil && c.parsedArgs != nil {
            t.Error("For", c.raw, "expected", c.parsedArgs, "got", args)
        }
        if len(args) != len(c.parsedArgs) {
            t.Error("For", c.raw, "expected", c.parsedArgs, "got", args)
        }
        for i := range args {
            if args[i] != c.parsedArgs[i] {
                t.Error("For", c.raw, "expected", c.parsedArgs, "got", args)
            }
        }
    }
}

type validatedTestCommand struct {
    r *robot.Robot
    args []string
    output error
}

var validatedTestCommands = []validatedTestCommand {
    // Invalid commands
    {
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 0,
                Y: 0,
            },
            Direction: 0,
        },
        []string{"MOVE"}, 
        robot.ErrRobotNotPlaced,
    },
    {
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 0,
                Y: 0,
            },
            Direction: 0,
        },
        []string{"LEFT"}, 
        robot.ErrRobotNotPlaced,
    },
    {
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 0,
                Y: 0,
            },
            Direction: 0,
        },
        []string{"RIGHT"}, 
        robot.ErrRobotNotPlaced,
    },
    {
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 0,
                Y: 0,
            },
            Direction: 0,
        },
        []string{"REPORT"}, 
        robot.ErrRobotNotPlaced,
    },

    // Valid commands
    {
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 0,
                Y: 0,
            },
            Direction: 0,
        },
        []string{"HELP"}, 
        nil,
    },
    {
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 0,
                Y: 0,
            },
            Direction: 0,
        },
        []string{"QUIT"}, 
        nil,
    },
    {
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 0,
                Y: 0,
            },
            Direction: 0,
        },
        []string{"PLACE", "0,0,NORTH"}, 
        nil,
    },
    {
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 0,
                Y: 0,
            },
            Direction: robot.Directions["NORTH"],
        },
        []string{"MOVE"},
        nil,
    },
    {
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 0,
                Y: 0,
            },
            Direction: robot.Directions["NORTH"],
        },
        []string{"LEFT"},
        nil,
    },
    {
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 0,
                Y: 0,
            },
            Direction: robot.Directions["NORTH"],
        },
        []string{"RIGHT"},
        nil,
    },
    {
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 0,
                Y: 0,
            },
            Direction: robot.Directions["NORTH"],
        },
        []string{"REPORT"},
        nil,
    },
}

// Test commands are ignored if robot not placed
func TestInitRobotPlacement(t *testing.T) {
    for _, c := range validatedTestCommands {
        r := robot.GetRobotStatus(c.r, c.args)
        // Check return value is as expected
        if r != c.output {
            t.Error("For", c.args, "expected", c.output, "got", r)
        }
    }
}

type testCommand struct {
    r *robot.Robot
    er *robot.Robot
    args []string
    output error
}

var testCommands = []testCommand {
    // Invalid commands
    {
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 4,
                Y: 4,
            },
            Direction: robot.Directions["NORTH"],
        },
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 4,
                Y: 4,
            },
            Direction: robot.Directions["NORTH"],
        },
        []string{"MOVE"}, 
        robot.ErrInvalidMove,
    },
    {
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 0,
                Y: 0,
            },
            Direction: robot.Directions["SOUTH"],
        },
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 0,
                Y: 0,
            },
            Direction: robot.Directions["SOUTH"],
        },
        []string{"MOVE"}, 
        robot.ErrInvalidMove,
    },
        
    // Valid commands
    {
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 0,
                Y: 0,
            },
            Direction: robot.Directions["NORTH"],
        },
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 0,
                Y: 1,
            },
            Direction: robot.Directions["NORTH"],
        },
        []string{"MOVE"}, 
        nil,
    },
    {
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 4,
                Y: 4,
            },
            Direction: robot.Directions["WEST"],
        },
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 3,
                Y: 4,
            },
            Direction: robot.Directions["WEST"],
        },
        []string{"MOVE"}, 
        nil,
    },
    {
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 1,
                Y: 1,
            },
            Direction: robot.Directions["NORTH"],
        },
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 1,
                Y: 1,
            },
            Direction: robot.Directions["EAST"],
        },
        []string{"RIGHT"}, 
        nil,
    },
    {
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 1,
                Y: 1,
            },
            Direction: robot.Directions["SOUTH"],
        },
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 1,
                Y: 1,
            },
            Direction: robot.Directions["EAST"],
        },
        []string{"LEFT"}, 
        nil,
    },
    {
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 2,
                Y: 4,
            },
            Direction: robot.Directions["WEST"],
        },
        &robot.Robot{
            Coordinate: robot.Coordinate{
                X: 3,
                Y: 1,
            },
            Direction: robot.Directions["NORTH"],
        },
        []string{"PLACE", "3,1,NORTH"}, 
        nil,
    },
}

// Test commands are processed correctly
func TestProcessCommand(t *testing.T) {
    for _, c := range testCommands {
        r := robot.ProcessCommand(c.r, c.args)
        // Check return value is as expected
        if r != c.output {
            t.Error("For", c.args, "expected", c.output, "got", r)
        }
        // Check robot is as expected
        if reflect.DeepEqual(c.r, c.er) == false {
            t.Errorf("For %v expected %+v got %+v", c.args, c.r, c.er)
        }
    }
}