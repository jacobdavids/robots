package validate

import (
    "errors"
    "strconv"
    "strings"

    "gitlab.com/jacobdavids/robots.git/robot"
    "gitlab.com/jacobdavids/robots.git/utils"
)

// Valid commands
var validCommands = []string{"PLACE", "MOVE", "LEFT", "RIGHT", "REPORT", "HELP", "QUIT"}

// Validation errors
var ErrInvalidCommand = errors.New("Invalid command. Use \"HELP\" to view available commands.")
var ErrInvalidCoordinates = errors.New("Invalid coordinates. Coordinates must be integers between 0 and 4.")
var ErrInvalidDirection = errors.New("Invalid direction. Direction must be either: NORTH, EAST, SOUTH, WEST.")

// Validate input commands and return error if command invalid
func ValidateCommand(args []string) error {
    // Check for invalid number of command arguments
    if (len(args) == 0 || len(args) > 2) {
        return ErrInvalidCommand
    }
    
    // Check command is valid
    command := args[0]
    if (utils.StringInSlice(command, validCommands) == false) {
        return ErrInvalidCommand
    }

    // Check correct number of arguments for commands
    if (((command == "MOVE" || command == "LEFT" || command == "RIGHT" || 
        command == "REPORT" || command == "HELP" || command == "QUIT") &&
        len(args) != 1) || ((command == "PLACE") && len(args) != 2)) {
        return ErrInvalidCommand
    }

    // Validate PLACE command
    if (command == "PLACE") {
        // Split PLACE command by comma
        placement := strings.Split(args[1], ",")

        // Check PLACE command has correct number of comma-seperated arguments
        if (len(placement) != 3) {
            return ErrInvalidCommand
        }

        // Check coordinates are integers
        X, err := strconv.Atoi(placement[0])
        if err != nil {
            return ErrInvalidCoordinates
        }
        Y, err := strconv.Atoi(placement[1])
        if err != nil {
            return ErrInvalidCoordinates
        }

        // Check coordinates are within min and max bounds
        err = ValidateCoordinates(X, Y)
        if err != nil {
            return err
        }

        // Validate direction
        F := placement[2]
        err = ValidateDirection(F)
        if err != nil {
            return err
        }
    }
    return nil
}

// Returns ErrInvalidCoordinates error if coordinates are not within min and max bounds
func ValidateCoordinates(X int, Y int) error {
    if (X < robot.MIN_COORDINATE || X > robot.MAX_COORDINATE || 
        Y < robot.MIN_COORDINATE || Y > robot.MAX_COORDINATE) {
        return ErrInvalidCoordinates
    }
    return nil
}

// Returns ErrInvalidDirection error if direction string is invalid
func ValidateDirection(F string) error {
    _, ok := robot.Directions[F]
    if (!ok) {
        return ErrInvalidDirection
    }
    return nil
}
