#!/bin/bash
TESTDIR="tests"
TESTFILES="test1
test2
test3
test4
test5
test6
test7
test8
test9
test10"

echo "Running unit tests..."
go test

echo "-------------------------------------------------------"

echo "Running functional tests..."
TESTSPASSED=true
for TEST in $TESTFILES
do
    TESTDIFF=$(./robots.git < "$TESTDIR/$TEST.input" | diff "$TESTDIR/$TEST.output" -)

    if [ "$TESTDIFF" != "" ]
    then
        echo "$TEST failed."
        TESTSPASSED=false
    else
        echo "$TEST passed."
    fi
done

if [ "$TESTSPASSED" = true ]
then
    echo 'PASS'
else
    echo 'FAIL'
fi

echo "-------------------------------------------------------"