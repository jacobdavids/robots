# Robots

Robots: Toy Robot Simulator

Robots is a simulation of a toy robot moving on a square tabletop, of dimensions 5 units x 5 units. The project is written in the Go programming language.

## Running the project

To get Robots, simply run ```go get gitlab.com/jacobdavids/robots.git```. This downloads the project into your ```$GOPATH```.

Next, navigate to ```$GOPATH/src/gitlab.com/jacobdavids/robots.git``` and run the command ```go build```. This builds a Robots binary in the current directory.

To run the project, execute the binary by running ```./robots.git```.

## Using the project

The simulator works by entering commands via the command prompt. For a list of available commands, type ```HELP```.

## Testing the project

Testing is done via a bash script named ```test.sh``` in the root directory of the project. The script will run the unit tests located in the ```main_test.go``` file and also the functional tests located in the ```tests/``` directory. The functional tests work by piping the commands found in the ```testX.input``` files into the Robots binary and then comparing the outputs with the expected outputs found in the corresponding ```testX.output``` files.

To run the tests, execute the bash script by running ```bash test.sh```.

## License
```
Robots: Toy Robot Simulator

The MIT License (MIT)

Copyright (c) 2017 Jacob Davids

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software ("Robots: Toy Robot Simulator") and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
```