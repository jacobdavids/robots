package robot

import (
    "os"
    "errors"
    "fmt"
    "strconv"
    "strings"

    "gitlab.com/jacobdavids/robots.git/utils"
)

var ErrInvalidMove = errors.New("Command ignored. Robot cannot be moved off table.")
var ErrRobotNotPlaced = errors.New("Command ignored. Robot must be placed first with PLACE command.")

type Coordinate struct {
  X, Y int
}

type Robot struct {
    Coordinate
    Direction  int
}

var Directions = map[string]int {
    "NORTH": 1,
    "EAST": 2,
    "SOUTH": 3,
    "WEST": 4,
}

const (
    MAX_COORDINATE int = 4
    MIN_COORDINATE int = 0
)

// Returns a pointer to a new robot
func CreateRobot() *Robot {
    return &Robot{}
}

// Returns ErrRobotNotPlaced if robot has not been placed on table
func GetRobotStatus(r *Robot, args []string) error {
    command := args[0]
    // A robot that has not been placed will have a Direction of 0
    _, ok := utils.MapKeyFromValue(Directions, r.Direction)
    if (!ok && (command != "PLACE" && command != "HELP" && command != "QUIT")) {
        return ErrRobotNotPlaced
    }
    return nil
}

// Process and execute robot commands
func ProcessCommand(r *Robot, args []string) error {
    command := args[0]

    switch command {
        case "PLACE":
            // Split PLACE command by comma
            position := strings.Split(args[1], ",")
            // Place robot at specified position
            place(r, position)
        case "MOVE":
            // Move robot forward one unit
            err := move(r)
            if err != nil {
                return err
            }
        case "LEFT":
            // Rotate robot left
            rotateLeft(r)
        case "RIGHT":
            // Rotate robot right
            rotateRight(r)
        case "REPORT":
            // Get the direction string from value
            direction, _ := utils.MapKeyFromValue(Directions, r.Direction)
            // Print position and direction of robot
            fmt.Printf("\n%d,%d,%s\n\n", r.Coordinate.X, r.Coordinate.Y, direction)
        case "HELP":
            // Print help menu
            printHelpMenu()
        case "QUIT":
            // Quit simulator
            os.Exit(3)
    }
    return nil
}

// Place robot at specified position on table
func place(r *Robot, position []string) {
    // Convert coordinates to integers
    X, _ := strconv.Atoi(position[0])
    Y, _ := strconv.Atoi(position[1])

    // Set coordinates
    r.Coordinate.X = X
    r.Coordinate.Y = Y

    // Set direction
    F := position[2]
    r.Direction = Directions[F]
}

// Move robot one unit forward in current direction 
func move(r *Robot) error {
    tempX := r.Coordinate.X
    tempY := r.Coordinate.Y

    // Make temporary move
    if (r.Direction == Directions["NORTH"]) {
        tempY++
    } else if (r.Direction == Directions["EAST"]) {
        tempX++
    } else if (r.Direction == Directions["SOUTH"]) {
        tempY--
    } else if (r.Direction == Directions["WEST"]) {
        tempX--
    }

    // Check if move was valid
    if (tempX < MIN_COORDINATE || tempX > MAX_COORDINATE || 
        tempY < MIN_COORDINATE || tempY > MAX_COORDINATE) {
        return ErrInvalidMove
    }

    // Move was valid, make move
    r.Coordinate.X = tempX
    r.Coordinate.Y = tempY

    return nil
}

// Rotate robot in left direction
func rotateLeft(r *Robot) {
    if (r.Direction == Directions["NORTH"]) {
        r.Direction = Directions["WEST"]
    } else {
        r.Direction--
    }
}

// Rotate robot in right direction
func rotateRight(r *Robot) {
    if (r.Direction == Directions["WEST"]) {
        r.Direction = Directions["NORTH"]
    } else {
        r.Direction++
    }
}

// Print help menu
func printHelpMenu() {
    fmt.Println("")
    fmt.Println("Available commands are:")
    fmt.Println("")
    fmt.Println("PLACE X,Y,F e.g. PLACE 0,0,NORTH")
    fmt.Println("MOVE")
    fmt.Println("LEFT")
    fmt.Println("RIGHT")
    fmt.Println("REPORT")
    fmt.Println("QUIT")
    fmt.Println("")
}