package utils

// Returns true if string is found in slice
func StringInSlice(a string, list []string) bool {
    for _, b := range list {
        if b == a {
            return true
        }
    }
    return false
}

// Returns key string for value found in map
func MapKeyFromValue(m map[string]int, value int) (key string, ok bool) {
    for k, v := range m {
        if v == value { 
          key = k
          ok = true
          return
        }
    }
    return
}